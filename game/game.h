#include <string>
#include <cstring>

#include <iostream>
#ifndef GAME_H
#define GAME_H

using namespace std;

class Game{
public:

    string name;
    int* highscores;

    Game(string name) : name(name), highscores(nullptr){

     }
    //Labb 5.6
    ~Game(){

       delete []highscores;
    }
    //Labb 5.7
    Game(const Game & game){

       highscores = new int[10];
       for (int i = 0; i < 10; i++){
           highscores[i] = game.getScore(i);
       }

    }
    //Labb 5.8
    Game & operator=(Game &game){

        delete []highscores;
        highscores = new int[10];
        for (int i = 0; i < 10; i++){
            highscores[i] = game.getScore(i);
        }
        return *this;

    }

     void addScore(int score){
     // Create the highscore array if it's null.
     if (highscores == nullptr){

     highscores = new int[10];

     for (int i = 0; i < 10; i++){
     highscores[i] = 0;
     }
     }
     // Find the right index for the new score.
     int scoreIndex = -1;
     for (int i = 0; i < 10; i++){
     if (highscores[i] < score){
     scoreIndex = i;
     break;
     }
     }
     if (scoreIndex != -1){
     // Move down lower scores.
     for (int i = 9; scoreIndex < i; i--){
     highscores[i] = highscores[i - 1];
     }
     // Insert this score.
     highscores[scoreIndex] = score;
     }

   }
     int getScore(int place) const {
      if(highscores == nullptr){
      return 0;
      }else{
       return highscores[place];
       }
       }
};

#endif // GAME_H
