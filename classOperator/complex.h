#ifndef COMPLEX_H
#define COMPLEX_H


class Complex
{
public:
    Complex(){}

    Complex(int num){
         this->num = num;
    }
    int get_num(){
        return this->num;
    }
    Complex operator+(const Complex &right){

        return this->num + right.num;
    }
    Complex operator-(const Complex &right){

        return this->num - right.num;
    }
    Complex operator<<(const Complex &right){

        return this->num << right.num;
    }

private:
    int num;
};

#endif // COMPLEX_H
