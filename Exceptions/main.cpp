#include <iostream>
#include <stdio.h>
#include <regex>
#include <stdlib.h>

using namespace std;

int compute(int leftO, char symbol, int rightO);

int main(int argc, char *argv[]){

 char leftO;
 char symbol;
 char rightO;

 cout << "Welcome to the compute program!" << endl;

 while (true){

     cout << "Enter left operand (an integer): ";
     cin >> leftO;
     cout << "Enter operation symbol (+, - / or *): ";
     cin >> symbol;
     cout << "Enter right operand (an integer): ";
     cin >> rightO;


     try {
         int result = compute(leftO, symbol, rightO);
         cout << leftO << " " << symbol << " " << rightO << " = " << result << endl;
     } catch (int e) {

        switch(e){
            case 0:
            cout << ">>fel datatyp, testa igen!<<" << endl;
            break;
            case 1:
            cout << ">>fel operator symbol, testa igen!<<" << endl;
            break;
            case 2:
            cout << ">>Kan inte dela med noll, testa igen!<<" << endl;
            break;
        }
     }

 }
}

int compute(int leftO, char symbol, int rightO){

if(!isdigit(leftO) || !isdigit(rightO) || isdigit(symbol)){
    throw 0;
}
if(symbol != '-' && symbol != '+' && symbol != '/' && symbol != '*'){
    throw 1;
}
if(rightO == '0' && symbol == '/'){
    throw 2;
}

int lo = leftO - '0';
int ro = rightO - '0';


    switch(symbol){
        case '+':
            return lo + ro;
        case '-':
            return lo - ro;
        case '/':
            return lo / ro;
        case '*':
            return lo * ro;
    }
}
