#include "threedpoint.h"
#include <math.h>



template<class T>
ThreeDPoint<T>::ThreeDPoint()
{
    this->z = 0;
}
template<class T>
ThreeDPoint<T>::ThreeDPoint(const T x, const T y, const T z):Point<T>(x,y)
{
    this->z = z;
}

template<class T>
T ThreeDPoint<T>::getZ() const
{
    return z;
}

template<class T>
void ThreeDPoint<T>::setZ(const T newZ)
{
    z = newZ;
}
template<class T>
bool ThreeDPoint<T>::isSameAs(const ThreeDPoint *otherPoint) const
{

    if (Point<T>::isSameAs(otherPoint) && z == otherPoint->getZ())
    {
        return true;
    }
    else
    {
        return false;
    }
}

template<class T>
void ThreeDPoint<T>::setTo(const ThreeDPoint &otherPoint)
{
    Point<T>::setTo(otherPoint);
    z = otherPoint.getZ();
}
template<class T>
T ThreeDPoint<T>::getDistanceTo(const ThreeDPoint otherPoint) const
{
    T answer = sqrt(pow(Point<T>::getDistanceTo(otherPoint),2) + pow(otherPoint.getZ() - z,2));
    return answer;
}

template<class T>
void ThreeDPoint<T>::changeZ(const T dz)
{
    z += dz;
}
