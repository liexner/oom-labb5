#include "point.h"
#include "threedpoint.h"
#include <iostream>
using namespace std;

int main(void){

    Point <float> myPoint(5,5);
    cout << myPoint.getX() << endl;

    ThreeDPoint <float> my3Dpoint(5,6,8);
    cout << my3Dpoint.getZ() << endl;

}
