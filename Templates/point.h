#pragma once
#include <math.h>

template<class T> class Point{

    public:

        Point();
        Point(const T x, const T y);
        T getX() const;
        T getY() const;
        void setX(const T newX);
        void setY(const T newY);
        bool isSameAs(const Point *otherPoint) const;
        void setTo(const Point &otherPoint);
        T getDistanceTo(const Point otherPoint) const;
        void changeX(const T dx);
        void changeY(const T dy);

    protected:
        double x;
        double y;

    };


template<class T>
Point<T>::Point()
{
     x = 0;
     y = 0;
}
template<class T>
Point<T>::Point(const T x, const T y)
{
        this->x = x;
        this->y = y;
}

template<class T>
T Point<T>::getX() const
{
    return x;
}

template<class T>
T Point<T>::getY() const
{
    return y;
}

template<class T>
void Point<T>::setX(const T newX)
{
    x = newX;
}

template<class T>
void Point<T>::setY(const T newY)
{
    y = newY;
}

template<class T>
bool Point<T>::isSameAs(const Point *otherPoint) const
{

    if (x == otherPoint->getX() && y == otherPoint->getY())
    {
        return true;
    }
    else
    {
        return false;
    }
}

template<class T>
void Point<T>::setTo(const Point &otherPoint)
{
    x = otherPoint.getX();
    y = otherPoint.getY();
}

template<class T>
T Point<T>::getDistanceTo(const Point otherPoint) const
{
    double answer = sqrt(pow(otherPoint.getX() - x,2) +  pow(otherPoint.getY() - y,2));
    return answer;
}

template<class T>
void Point<T>::changeX(const T dx)
{
    x += dx;
}


template<class T>
void Point<T>::changeY(const T dy)
{
    y += dy;
}



