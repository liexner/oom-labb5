#include <iostream>
#include <chrono>
#include <thread>
#include "game.h"
using namespace std;

void labb_56();
void labb_57();
void labb_58();
int getSumOfScores(Game game);

int main()
{

    labb_56();
    labb_57();
    labb_58();



}
void labb_58(){

    Game gameA("Mario");
    gameA.addScore(10);
    gameA.addScore(22);
    cout << getSumOfScores(gameA) << endl;
    Game gameB("Luigi");
    gameB.addScore(30);
    cout << getSumOfScores(gameB) << endl;
    gameB = gameA;
    cout << getSumOfScores(gameA) << endl;
    cout << getSumOfScores(gameB) << endl;


}
void labb_57(){
    Game game("Mario");
    game.addScore(10);
    game.addScore(20);
    game.addScore(30);
    int sumOfScores = getSumOfScores(game);
    cout << sumOfScores << endl;
}
void labb_56(){

    while (true){

     Game game("Mario");
     game.addScore(10);

     // Do nothing for 1 milisecond, then continue.
     this_thread::sleep_for(std::chrono::milliseconds(1));

     }


}
int getSumOfScores(Game game){
 int sum = 0;
 for(int i=0; i<10; i++){

 sum += game.getScore(i);
 }
 return sum;
}
